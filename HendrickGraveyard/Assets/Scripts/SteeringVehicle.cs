﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Steer))]
[RequireComponent(typeof(CharacterController))]

public class SteeringVehicle : MonoBehaviour {

//movement variables - exposed in inspector panel
	private WayPoint target;

	public float gravity = 80000.0f; // keep us grounded
	
	protected CharacterController body;
	
	protected int ID;
	
	protected static int nextID = 0;
	
	// the SteeringAttributes holds several variables needed for steering
	protected SteeringAttributes attr;

	// the Steer component implements the basic steering functions
	protected Steer steer;
	
	private int maxSpeedOffset;
	
	protected GameManager gameManager;
	
	private Vector3 acceleration;	//change in velocity per second
	private Vector3 velocity;		//change in position per second
	public Vector3 Velocity {
		get { return body.velocity; }
		set { velocity = value;}
	}

	public WayPoint Target {
		get { return target; }
		set { target = value;}
	}
	
	protected virtual void Start ()
	{	
		ID = nextID++;
		acceleration = Vector3.zero;
		velocity = transform.forward;
		
		//get component references
		body = gameObject.GetComponent<CharacterController> ();
		steer = gameObject.GetComponent<Steer> ();
		GameObject main = GameObject.Find("MainGO");
		attr = main.GetComponent<SteeringAttributes> ();
		gameManager = main.GetComponent<GameManager>();
		
		maxSpeedOffset = Random.Range (-5, 20);
	}
	
	protected virtual void Update ()
	{		
		CalcSteeringForce ();
		
		//update velocity
		velocity += acceleration * Time.deltaTime;
		velocity.y = 0;	// we are staying in the x/z plane
		
		Debug.DrawRay (transform.position, acceleration, Color.red);
		
		velocity = Vector3.ClampMagnitude (velocity, attr.maxSpeed + maxSpeedOffset);
		
		Debug.DrawRay (transform.position, velocity, Color.cyan);
		
		//orient the transform to face where we going
		if (velocity != Vector3.zero)
			transform.forward = velocity.normalized;
		
		// keep us grounded
		velocity.y -= gravity * Time.deltaTime;
		
		body.Move(velocity * Time.deltaTime);
		
		//reset acceleration for next cycle
		acceleration = Vector3.zero;
	}
	
	//calculate and apply steering forces
	private void CalcSteeringForce ()
	{ 
		Vector3 force = Vector3.zero;
		
		force += calculateCustomSteeringForces();
		
		force = Vector3.ClampMagnitude (force, attr.maxForce);
		
		ApplyForce(force);
	}
	
	protected virtual Vector3 calculateCustomSteeringForces(){
	    return Vector3.zero;
	}
	
	protected void ApplyForce (Vector3 steeringForce)
	{
		acceleration += steeringForce/attr.mass;
	}
	
	public int getID(){
	    return ID;	
	}
}
