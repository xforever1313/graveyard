The Haunted Cemetery
Seth Hendrick
Interactive Media Development Final Unity Project

To see in in action:
http://files.shendrick.net/rit/imd/ghosts/

----------------------------
Description:
On a dark and clear evening, ghosts decided to rise from the graves and scare innocent people.  However, after the long night, they must return to their graves and slumber.

This scene shows the ghosts teleporting outside the cemetery from wherever they came from.  For some unknown reason, they are unable to teleport inside of the cemetery.  The ghosts will then queue with other ghosts to enter the cemetery entrance.  Once through the entrance, the ghosts will figure out the quickest path to their graves, and then take it.  When they are near their grave, they will climb on the mound before it, and then fade away.  When one ghost fades, another will spawn outside of the cemetery.

Each grave has a candle on it.  The candle's flame is a random color.  When a ghost spawns, it has a light surrounding it.  The light matches its grave's candle.  Therefore, the ghosts will go to the grave whose candle's color matches its own glow.

----------------------------
Instructions:
Left/Right arrows: change the ghost cam to another ghost.  Must not be in overhead mode
Space Bar: go to overhead mode and see the whole cemetery.  Press the space bar again to go return to ghost cam mode.

----------------------------
Implementation notes:
Ghosts are a state machine that have 5 states: queueing, finding, mounding, disappearing, and sleeping.

When the ghosts are in the queue state, the ghosts will seek the cemetery entrance.  For each ghost, if there's a nearby ghost in front of them that is going slower than they are, they will slow down so that they do no bump into each other.  The ghosts will also separate from each other if they are too close.  Ghosts will also avoid the outer walls of the cemetery.  All ghosts move at different speeds.

When the ghosts get close enough to the cemetery gate, they switch to the finding state.  The ghost calculates the best path for it to travel using the A* algorithm, and then follows that path.  Also in this state, the ghosts use separation so they do not run into each other.  When the ghosts get to a point on the path that is next to their grave, they enter the mounding state.  While the ghosts are in this state, they will remain on the path.

The mounding state is simply when the ghosts goes off the path and onto its grave's mound.  It does this by simply seeking the mound's WayPoint.

When the ghost reaches the center of its mound, it then enters the disappearing state.  Every frame, the ghost's body starts to fade, and the ghost's halo gets smaller and smaller until the ghost is gone.  Once the radius of the halo gets small enough, the ghost enters the sleeping stage.

The sleeping stage is basically a reset stage.  The ghost's alpha and halo are restored, its position is randomly generated in the spawn area, and its target changes.  The state returns to the queueing state.


In order to make the A* algorithm easier, the cemetery was set up like a grid.  The grid has several types of spaces:
Dummy - an empty game object, used as a filler space.
Path - where ghosts can walk
Mound - where the ghosts end up
Head store - where the grave's headstone is located.
Wall - a wall to the cemetery.

Instead of manually laying down each cemetery piece in unity's scene tool, the cemetery is auto generated through code.  This was done so if a changed needed to be made to the cemetery layout, it could be done by typing a few keys, instead of dragging things around in Unity.  The cemetery's layout is determined by a string, and each character in the string determines what the space in the cemetery is.  All waypoints, targets for the ghosts, walls, paths, and which mound goes with which target are all saved so they can be easily accessed by other classes.

Each grid in the cemetery is a waypoint.  A WayPoint contains a gameObject, and therefore, a position.  It also is aware of its neighbours, what kind of space in the cemetery it is (Path, grave, dummy, etc), has a unique ID, and various other useful members.

-------------------
Other Notes:
The camera strategy is different than what was presented during the "this is my idea" day in class.  The original idea was that a first person controlled camera was going to be used, in addition to the overhead view.  However, the ghost cam idea was chosen instead of the first person controlled camera, as it seems to "flow" better.  The overhead view remains in the final version.

-------------------
Sources
All trees, and terrain colors were gotten from Unity's built-in standard assets and tree maker packages.  All other prefabs were made using primitive types.