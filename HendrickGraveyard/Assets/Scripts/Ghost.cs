﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ghost : SteeringVehicle {
	
	public static Vector3 getNextSpawnPoint(){
		float x = (Random.Range(475, 650));
		float z = (Random.Range (400, 475));
		float y = Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z));
		return new Vector3(x, y + 2, z);
	}

	private enum State{
	    QUEUEING,
		FINDING,
		MOUNDING,
		DISAPPEARING,
		SLEEPING
	}
	
	WayPoint moundTarget;
	WayPoint pathNextToMound;
	
	State state;
	
	Path path;
	
	int pathPosition;
	
	public int maxLightRange = 20;
	public int maxAlpha = 200;
	
	Light halo;
	
	Shader shade;
	
	Color ghostColor;
	int currentAlpha;
	
	protected override void Start(){
	    base.Start ();
		ghostColor = renderer.material.color; 
		halo = (Light)GetComponent("Light");
		respawn();
	}
	
	public void updateMound(){
		List<WayPoint> targets = gameManager.getCemetery().getPossibleTargets();
		int target = Random.Range(0, targets.Count);
	    pathNextToMound = targets[target];
		moundTarget = gameManager.getCemetery().getTargetMound(pathNextToMound);
	}
	
	// Update is called once per frame
	protected override void Update () {
	    if ((state == State.QUEUEING) && Target.isNearWayPoint(transform.position, Cemetary.GRID_RADIUS)){
		    state = State.FINDING;
			Target = pathNextToMound;
			
			path = steer.findPath(gameManager.getCemetery(), Target);
		}
		else if ((state == State.FINDING) && Target.isNearWayPoint(transform.position, Cemetary.GRID_RADIUS / 2)){
		    state = State.MOUNDING;
			Target = moundTarget;
		}
		else if ((state == State.MOUNDING) && Target.isNearWayPoint(transform.position, Cemetary.GRID_RADIUS / 2)){
			Velocity = Vector3.zero;
		    state = State.DISAPPEARING;
		}
		else if ((state == State.DISAPPEARING) && (halo.range <= 0.2)){
			state = State.SLEEPING;
		}
	}
	
	void LateUpdate()
	{	
		base.Update();
	}
	
	Vector3 queue(){
	    Vector3 ret = Vector3.zero;
		
		ret += steer.queue (Target.getPosition(), gameManager.getDistancesBetweenGhostsArray(ID), gameManager.getGhosts());
		
		List<WayPoint> walls = gameManager.getCemetery().getWalls();
		
		//Avoid walls
		foreach (WayPoint w in walls){
		    ret += attr.wallAvoidWeight * (steer.AvoidObstacle(w.getGameObject(), attr.wallAvoidDistance, Cemetary.GRID_RADIUS / 2));
		}
		
		return ret;
	}
	
	Vector3 find(){
	    Vector3 ret = Vector3.zero;
		
		//ret += steer.follow(path, curWp);
		if (pathPosition == path.getNumberOfWayPoints() -1){
			ret += attr.approachWeight * steer.approach(path.getWayPointAtIndex(pathPosition).getPosition());
		}
		else{
		    ret += steer.Seek (path.getWayPointAtIndex(pathPosition).getPosition());
			if (path.getWayPointAtIndex(pathPosition).isNearWayPoint(transform.position, 2)){
		        ++pathPosition;	
		    }
		}
		
		return ret;
	}
	
	Vector3 mound(){
		return steer.Seek(Target.getPosition());
	}
	
	void disappear(){
		halo.range -= (1/30f);
		float a = renderer.material.color.a;
		a -= 0.005f;
		renderer.material.color = new Color(ghostColor.r, ghostColor.r, ghostColor.b, a);
	}
	
	void respawn(){
	    transform.position = getNextSpawnPoint();
		updateMound();
		path = null;
		//curWp = null;
		pathPosition = 0;
	    state = State.QUEUEING;
		Target = gameManager.getCemetery().getGate();
		halo.range = maxLightRange;
		halo.color = moundTarget.getColor();
		renderer.material.color = ghostColor;
	}
	
	protected override Vector3 calculateCustomSteeringForces(){
	    Vector3 ret = Vector3.zero;
		
		if (state == State.QUEUEING){
		    ret += queue ();
			ret += attr.separationWt * steer.Separation(gameManager.getDistancesBetweenGhostsArray(ID), gameManager.getGhosts());
		}
		
		else if (state == State.FINDING){
		    ret += attr.findWeight * find ();
			ret += attr.separationWt * steer.Separation(gameManager.getDistancesBetweenGhostsArray(ID), gameManager.getGhosts());
		}
		
		else if (state == State.MOUNDING){
		    ret += attr.moundWeight * mound ();	
			ret += attr.separationWt * steer.Separation(gameManager.getDistancesBetweenGhostsArray(ID), gameManager.getGhosts());
		}
		
		else if (state == State.DISAPPEARING){
		    disappear ();	
		}
		
		else if (state == State.SLEEPING){
			respawn ();
		}
		
		return ret;
	}
}
