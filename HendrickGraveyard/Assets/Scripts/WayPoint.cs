﻿using UnityEngine;
using System.Collections;

public class WayPoint {
	
	public enum WPType{
	    WALL,
		PATH,
		UNWALKABLE,
		DUMMY
	}
	
	private GameObject go;
	
	public readonly int ID;
	
	WayPoint nextWayPoint;
	
	private float distanceToNextWayPoint;
	private Vector3 vectorToNextWayPoint;
	private Vector3 normalizedVectorToNextWayPoint;
	
	WayPoint northWayPoint;
	WayPoint southWayPoint;
	WayPoint eastWayPoint;
	WayPoint westWayPoint;
	
	private WPType type;
	
	public int cemetaryRow, cemetaryCol;
	
	public int fScore, gScore; //Used for path finding
	
	private Color color;
	
	public WayPoint(GameObject go, int id, WPType type){
		this.go = go;
		ID = id;
		distanceToNextWayPoint = 0;
		nextWayPoint = null;
		this.type = type;
		
	    northWayPoint = null;
	    southWayPoint = null;
	    eastWayPoint = null;
	    westWayPoint = null;
		
		cemetaryRow = -1;
		cemetaryCol = -1;
		
		fScore = int.MaxValue;
		gScore = 0;
		
		color = Color.white;
		
	}

	//Only valid if the waypoint's gameObject has a light
	public void generateAndSetRandomColor(){
	    this.color = new Color(Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
	}
	
	
	//Only valid if the waypoint's gameObject has a light
	public void setLightColor(Color c){
		this.color = c;
		Light flame = go.GetComponentInChildren<Light>();
		if (flame == null){
		    throw new UnityException("Not a waypoint with a light.  WayPoint ID: " + ID);
		}
		flame.color = c;
		flame.range = GameManager.candleRange;
	}
	
	public Color getColor(){
	    return color;	
	}
	
	public bool isNearWayPoint(Vector3 v, float radius){
		bool ret = false;
	    if (Vector3.Distance(v, go.transform.position) < radius){
		    ret = true;	
		}
		return ret;
	}
	
	public void setCemetaryCoords(int row, int col){
	    cemetaryRow = row;
		cemetaryCol = col;
	}
	
	public Vector3 getPosition(){
	    return go.transform.position;	
	}
	
	public GameObject getGameObject(){
	    return go;	
	}
	
	//Neighbor setters
	public void setNorthWayPoint(WayPoint other){
	    northWayPoint = other;	
	}
	
    public void setSouthWayPoint(WayPoint other){
	    southWayPoint = other;	
	}
	
	public void setEastWayPoint(WayPoint other){
	    eastWayPoint = other;	
	}
	
	public void setWestWayPoint(WayPoint other){
	    westWayPoint = other;	
	}
	
	public void setNextWaypoint(WayPoint other){
		nextWayPoint = other;
		distanceToNextWayPoint = Vector3.Distance(go.transform.position, other.go.transform.position);
		vectorToNextWayPoint = other.go.transform.position - go.transform.position;
		normalizedVectorToNextWayPoint = vectorToNextWayPoint.normalized;
	}
	
	//Neightbor getters
	public WayPoint getNorthWayPoint(){
	    return northWayPoint;	
	}
	
    public WayPoint getSouthWayPoint(){
	    return southWayPoint;	
	}
	
	public WayPoint getEastWayPoint(){
	    return eastWayPoint;	
	}
	
	public WayPoint getWestWayPoint(){
	    return westWayPoint;	
	}
	
	public float getDistanceToNextWayPoint(){
	    return distanceToNextWayPoint;	
	}
	
	public Vector3 getVectorToNextWayPoint(){
	    return vectorToNextWayPoint;	
	}
	
	public Vector3 getNormalizedVectorToNextWayPoint(){
	    return normalizedVectorToNextWayPoint;	
	}
	
	public WayPoint getNextWayPoint(){
	    return nextWayPoint;	
	}
	
	public WPType getType(){
	    return type;	
	}
	
	public WayPoint copy(int ID){
	    return new WayPoint(this.go, ID, this.type);
	}
}
