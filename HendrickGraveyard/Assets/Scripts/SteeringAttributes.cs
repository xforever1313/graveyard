﻿using UnityEngine;
using System.Collections;

public class SteeringAttributes : MonoBehaviour {
	
	//these are common attributes required for steering calculations 
	public int maxForce = 30;
	public int maxSpeed = 100;
	public int mass = 1;
	public int radius = 5;
	
// These weights will be exposed in the Inspector window

	public int seekWt = 70;
    public int separationWt = 30;
	public int separationDist = 15;
	public int pathRadius = 10;
	
	public int queueDistance = 15;
	public int queueWeight = 20;
	
	public int wallAvoidWeight = 30;
	public int wallAvoidDistance = 20;
	
	public int findWeight = 30;
	public int findCost = 10;
	
	public int moundWeight = 300;
	
	public int approachWeight = 1000;
	public int approachDivide = 10;
}
