﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	
	public int NUMBER_OF_GHOSTS = 5;
	
	public GameObject ghostPrefab;
	
	public GameObject corner;
	
	public GameObject pathPrefab;
	
	public GameObject unWalkablePrefab;
	
	public GameObject targetPrefab;
	
	public GameObject moundPrefab;
	
	public GameObject vertWallPrefab;
	
	public GameObject hortzWallPrefab;
	
	public GameObject cornerWallPrefab;
	
	public GameObject vertGrave;
	
	public GameObject hortzGrave;
	
	public static int candleRange = 2;
	
	List<GameObject> ghosts;
	
	int ghostWhoHasCamera;
	
	float [][] ghostDistances;
	
	public Camera overheadCamera;
	private Camera mainCamera;
	
	public GUIText ghostText;
	
	public GameObject dummyGO;
	
	private Cemetary cem;
	
	// Use this for initialization
	void Start () {
		cem = new Cemetary(this, corner.transform.position);
		
		ghosts = new List<GameObject>();
		
		for (int i = 0; i < NUMBER_OF_GHOSTS; ++i){
			GameObject temp = (GameObject)GameObject.Instantiate(ghostPrefab, Ghost.getNextSpawnPoint(), Quaternion.identity);
			ghosts.Add (temp);
		}
		
		//Allocate for ghost distances
		ghostDistances = new float [ghosts.Count][];
		for (int i = 0; i < ghosts.Count; ++i){
			ghostDistances[i] = new float[ghosts.Count];
		}

		ghostWhoHasCamera = 0;
		overheadCamera.enabled = false;
		Camera.main.GetComponent<SmoothFollow>().target = ghosts[ghostWhoHasCamera].transform;
		mainCamera = Camera.main;
	}
	
	private void updateGhostDistance(){
		for (int i = 0; i < ghosts.Count; ++i){
			for (int j = i; j < ghosts.Count; ++j){
				if (i == j){
					ghostDistances[i][j] = 0;
				}
				else{
			        ghostDistances[i][j] = Vector3.Distance(ghosts[i].transform.position, ghosts[j].transform.position);
				    ghostDistances[j][i] = ghostDistances[i][j];
				}
			}
		}
	}
	
	//For debugging only
	private void testghostDistances(){
        for (int i = 0; i < ghosts.Count; ++i){
		    string row = "";
		    for (int j = 0; j < ghosts.Count; ++j){
			   row += ghostDistances [i] [j] + " ";
			}
			Debug.Log(row);
		}
		throw new UnityException("DONE");
	}
	
	// Update is called once per frame
	void Update () {
		if (!overheadCamera.enabled){
			if (Input.GetKeyDown(KeyCode.RightArrow)){
				incrementCamera ();
			}
			else if (Input.GetKeyDown(KeyCode.LeftArrow)){
				decrementCamera();
			}
			else if (Input.GetKeyDown(KeyCode.Space)){
			    swapCamera();	
			}
			ghostText.enabled = true;
			ghostText.text = "Ghost Cam: " + ghostWhoHasCamera;
		}
		else if (Input.GetKeyDown (KeyCode.Space)){
		    swapCamera();	
		}
		else if (overheadCamera.enabled){
			ghostText.text = "Overhead Cam";
		}
		
		updateGhostDistance();
	}

	private void swapCamera(){
	    mainCamera.enabled = !mainCamera.enabled;
		overheadCamera.enabled = !overheadCamera.enabled;
	}
	
	private void incrementCamera(){
	    ++ghostWhoHasCamera;
		if (ghostWhoHasCamera == ghosts.Count){
		    ghostWhoHasCamera = 0;	
		}
		
		mainCamera.GetComponent<SmoothFollow>().target = ghosts[ghostWhoHasCamera].transform;
	}
	
    private void decrementCamera(){
		--ghostWhoHasCamera;
		if (ghostWhoHasCamera < 0){
		    ghostWhoHasCamera = ghosts.Count - 1;	
		}
		mainCamera.GetComponent<SmoothFollow>().target = ghosts[ghostWhoHasCamera].transform;
	}
	
	public float getDistancesBetweenghosts(int ghostID1, int ghostID2){
	    return ghostDistances[ghostID1] [ghostID2];	
	}
	
	public float [] getDistancesBetweenGhostsArray(int ghostID){
	    return ghostDistances[ghostID];	
	}
	
	public List<GameObject> getGhosts(){
	    return ghosts;	
	}
	
	public Cemetary getCemetery(){
	    return cem;	
	}
}
