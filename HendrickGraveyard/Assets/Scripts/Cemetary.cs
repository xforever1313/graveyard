﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cemetary {
	
	public static readonly int GRID_RADIUS = 5;
	
    private static readonly int GRID_SIZE_HEIGHT = 20;
	private static readonly int GRID_SIZE_WIDTH = 27;
	
	WayPoint [,] points;
	
	List<WayPoint> walls;
	
	List<WayPoint> paths;
	
	List<WayPoint> targets;
	
	Dictionary<int, WayPoint> targetsMound;
	
	WayPoint gate; //entrance
	
	private bool isTarget(char c){
	    return (c == '>') || (c == '<') || (c == 'V') || (c == '^');	
	}
	
	private bool isMound(char c){
	    return (c == 'W') || (c == 'M') || (c == 'E') || (c == '3');	
	}
	
	public Cemetary(GameManager main, Vector3 leftCorner){
		
		points = new WayPoint [GRID_SIZE_HEIGHT,GRID_SIZE_WIDTH];
		walls = new List<WayPoint>();
		paths = new List<WayPoint>();
		targets = new List<WayPoint>();
		targetsMound = new Dictionary<int, WayPoint>();
		
		string layout;
		//X = unwalkable
		//H = hortizontal headstone
		//I = vertical headstone
		//ME3W = Mound.  The direction of the mound is the 
		//P = Path
		//<>^V = which was is mound from path
		//| Vertical wall
		//- horizontal wall
		//0 - corner rotated by zero
		//9 - corner rotated by 90
		//1 - corner rotated by 180
		//2 - corner rotated by 270
		
		        //123456789012345678901234567
	    layout = "D2------------------0DDDDDD";  //R1
		layout +="D|XX3IXXXXXXXXXXXX3I1-----0";  //2
		layout +="D|XP^PPPPPPPPP>3IP^PPPPPPP|";  //3
		layout +="D|XPPVVVVVVVVPPIE<PPVVVVVV|";  //4
		layout +="D|XPPWWWWWWWWP>3IPPPWWWWWW|";  //5
		layout +="D|XPPHHHHHHHHPPIE<PVHHHHHH|";  //6
		layout +="D|XP>3IP>3IPPPPPPPPW2-----9";  //7
		layout +="-9XPPIE<>3IPPPPPPPPH|DDDDDD";  //8
		layout +="PPPPPIE<PIE<PP>3IPPX|DDDDDD";  //9
		layout +="-0XPPIE<>3IPPP>3IPPX|DDDDDD";  //10
		layout +="D|XPPPPP>3IHHPPPPPPX|DDDDDD";  //11
		layout +="D|HPPPPP>3IMMPPPPPPH1-----0";  //12
		layout +="D|M<>3IP>3I^^P>3IPPMXXXXXX|";  //13
		layout +="D|XP>3IPPIE<PPPIE<P^PPPPPP|";  //14
		layout +="D|XP>3IP>3IPPHHIE<PPPVVVVV|";  //15
		layout +="D|XP>3IP>3IPPMMIE<>3IWWWWW|";  //16
		layout +="D|XPPPPPPPPPP^^PPP>3IHHHHH|";  //17
		layout +="D|XPPPPPPPPPPPPPPPVX2-----9";  //18
		layout +="D|XXXXXXXXXXXXXXXX3I|DDDDDD";  //19
		layout +="D1------------------9DDDDDD";  //20;
		
		int stringIndex = 0;
		for (int i = 0; i < GRID_SIZE_HEIGHT; ++i){
		    for (int j = 0; j < GRID_SIZE_WIDTH; ++j){
				GameObject go = null;
				WayPoint.WPType type = WayPoint.WPType.DUMMY;
				List<WayPoint> listToAdd = null;
				if (layout[stringIndex] == 'X'){
				    go = main.unWalkablePrefab;
					type = WayPoint.WPType.UNWALKABLE;
				}
				else if (layout[stringIndex] == 'P'){
				    go = main.pathPrefab;	
					type = WayPoint.WPType.PATH;
					listToAdd = paths;
				}
				else if (layout[stringIndex] == '|'){
				    go = main.vertWallPrefab;
					type = WayPoint.WPType.WALL;
					listToAdd = walls;
				}
				else if (layout[stringIndex] == '-'){
				    go = main.hortzWallPrefab;
					type = WayPoint.WPType.WALL;
					listToAdd = walls;
				}
				else if (layout [stringIndex] == 'D'){
				    go = main.dummyGO;
					type = WayPoint.WPType.DUMMY;
					listToAdd = walls;
				}
				else if (isTarget(layout[stringIndex])){
				    go = main.targetPrefab;
					type = WayPoint.WPType.PATH;
					listToAdd = paths;
				}
				else if (isMound(layout[stringIndex])){
				    go = main.moundPrefab;
					type = WayPoint.WPType.UNWALKABLE;
				}
				else if (layout [stringIndex] == 'I'){
				    go = main.vertGrave;
					type = WayPoint.WPType.UNWALKABLE;
				}
				
				else if (layout [stringIndex] == 'H'){
				    go = main.hortzGrave;
					type = WayPoint.WPType.UNWALKABLE;
				}
				else if ((layout [stringIndex] == '0') || (layout [stringIndex] == '9') || (layout [stringIndex] == '1') || (layout [stringIndex] == '2')){
				    go = main.cornerWallPrefab;
					type = WayPoint.WPType.UNWALKABLE;
					listToAdd = walls;
				}
				else{
				    throw new UnityException("Invalid character: " + layout[stringIndex]);	
				}
				
				GameObject newGO = (GameObject)GameObject.Instantiate(go, new Vector3(leftCorner.x + (i * GRID_RADIUS), 0.1f, leftCorner.z + (j * GRID_RADIUS)), Quaternion.identity);
				
				//If a corner rotate
				if(layout [stringIndex] == '9'){
				    newGO.transform.Rotate(Vector3.up * 90);
				}
				else if (layout[stringIndex] == '1'){
					newGO.transform.Rotate(Vector3.up * 180);
				}
				else if (layout[stringIndex] == '2'){
					newGO.transform.Rotate(Vector3.up * 270);
				}
				
				points[i,j] = new WayPoint(newGO, stringIndex, type);
				
				if (listToAdd != null){
				    listToAdd.Add(points[i,j]);
				}
				
				if (i != 0){
				    points[i - 1, j].setSouthWayPoint(points[i, j]);
					points[i, j].setNorthWayPoint(points[i - 1, j]);
				}
				
				if (j != 0){
				    points[i, j-1].setEastWayPoint(points[i, j]);
					points[i, j].setWestWayPoint(points[i, j-1]);
				}
				
				points[i, j].setCemetaryCoords(i, j);
				
				++stringIndex;
			}
		}
		
		stringIndex = 0;
		for (int i = 0; i < GRID_SIZE_HEIGHT; ++i){
		    for (int j = 0; j < GRID_SIZE_WIDTH; ++j){
				
			    if (isTarget(layout[stringIndex])){
					targets.Add(points[i, j]);
					if (layout[stringIndex] == '>'){
					    targetsMound[points[i, j].ID] = points[i, j + 1];	
					}
					else if (layout[stringIndex] == '<'){
					    targetsMound[points[i, j].ID] = points[i, j - 1];	
					}
					else if (layout[stringIndex] == 'V'){
					    targetsMound[points[i, j].ID] = points[i + 1, j];	
					}
					else if (layout[stringIndex] == '^'){
					    targetsMound[points[i, j].ID] = points[i - 1, j];	
					}					
				}
				else if (isMound (layout[stringIndex])){
					if (layout[stringIndex] == 'E'){
					    points[i, j].generateAndSetRandomColor();
						points[i, j].getWestWayPoint().setLightColor(points[i, j].getColor());
					}
					else if (layout[stringIndex] == '3'){
					    points[i, j].generateAndSetRandomColor();
						points[i, j].getEastWayPoint().setLightColor(points[i, j].getColor());
					}
					else if (layout[stringIndex] == 'W'){
					    points[i, j].generateAndSetRandomColor();
						points[i, j].getSouthWayPoint().setLightColor(points[i, j].getColor());
					}
					else if (layout[stringIndex] == 'M'){
					    points[i, j].generateAndSetRandomColor();
						points[i, j].getNorthWayPoint().setLightColor(points[i, j].getColor());
					}	
				}
				++stringIndex;
			}
		}
		
		Vector3 v = points[8,0].getPosition();
		GameObject gateObject = (GameObject) GameObject.Instantiate(main.dummyGO, v, Quaternion.identity);
		gate = new WayPoint(gateObject, -1, WayPoint.WPType.PATH);
		gate.setNextWaypoint(points[8, 1]);
	}
	
	public List<WayPoint> getWalls(){
	    return walls;
	}
	
	public WayPoint [,] getPoints(){
		return points;
	}
	
	public List<WayPoint> getPath(){
		return paths;
	}
	
	public WayPoint getGate(){
	    return gate;	
	}
	
	public List<WayPoint> getPossibleTargets(){
	    return targets;	
	}
	
	public WayPoint getTargetMound(WayPoint target){
	    return targetsMound[target.ID];
	}
}
