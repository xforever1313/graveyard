﻿// The Steer component has a collection of functions
// that return forces for steering 

using UnityEngine;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(CharacterController))]

public class Steer : MonoBehaviour
{
	Vector3 dv = Vector3.zero; 	// desired velocity, used in calculations
	SteeringAttributes attr; 	// attr holds several variables needed for steering calculations
	CharacterController body;
	
	void Start ()
	{
		GameObject main = GameObject.Find("MainGO");
		attr = main.GetComponent<SteeringAttributes> ();
		body = gameObject.GetComponent<CharacterController> ();	
	}
	
	
	//-------- functions that return steering forces -------------//
	public Vector3 Flee (Vector3 targetPos)
	{
		//find dv, desired velocity
		dv = transform.position - targetPos;		
		dv = dv.normalized * attr.maxSpeed; 	//scale by maxSpeed
		dv -= body.velocity;
		return dv;
	}
	
	public Vector3 Seek(Vector3 targetPos){
		//find dv, desired velocity
		dv = targetPos - transform.position;		
		dv = dv.normalized * attr.maxSpeed; 	//scale by maxSpeed
		dv -= body.velocity;
		return dv;
	}
	
	public Vector3 approach(Vector3 targetPos){
	    Vector3 seekedVector = Seek (targetPos);
		Vector3 oppositeVector = -seekedVector.normalized;
		oppositeVector *= (1 / Vector3.Distance(targetPos, transform.position));
		seekedVector -= (oppositeVector);
		return seekedVector;
	}
	
	//These functions will be called from calcSteeringForce (in SteeringVelicle.cs)
	
	//CalcSteeringForce will get average direction of the flock from GameManager
	//Return the desired velocity
	public Vector3 Align(Vector3 direction){
		Vector3 desired = direction.normalized;
		desired *= attr.maxSpeed;
		desired = desired - body.velocity;
		return desired;
	}
	
	//CalcSteeringForce will get centroid from GameManager
	//Return the desired velocity
	public Vector3 Cohesion(Vector3 centroid){
		return Seek (centroid);
	}
	
	//CalcSteeringForce will get flockers from GameManager
	//Return the desired velocity
	public Vector3 Separation(float[] distancesToOtherFlockers, List<GameObject> flockers){
		Vector3 desired = Vector3.zero;
		
		int count = 0;
		
		for (int i = 0; i < distancesToOtherFlockers.Length; ++i){
			float distance = distancesToOtherFlockers[i];
			if (distance != 0){ //Don't do anything if its the same flocker
		        if (distance <= attr.separationDist){
					Vector3 fleeVector = transform.position - flockers[i].transform.position;
					fleeVector.Normalize();
					desired += fleeVector * (1/distance);
					++count;
				}
			}
		}
		
		if (count != 0){
		    desired /= count;
		}
		
		if(desired.sqrMagnitude != 0){
			desired.Normalize();
			desired *= attr.maxSpeed;
			desired = desired - body.velocity;
		}
		return desired;
	}
	
	private Vector3 getNormalPoint(WayPoint a, Vector3 prediction){
	    Vector3 ap = prediction - a.getPosition();
		
		Vector3 ab = a.getNormalizedVectorToNextWayPoint();
		ab *= Vector3.Dot (ap, ab);
		Vector3 normalPoint = a.getPosition() + ab;
		return normalPoint;
	}
	
	public Vector3 follow(Path path, WayPoint curWP){
        Vector3 predict = new Vector3 (body.velocity.x, body.velocity.y, body.velocity.z);
        predict.Normalize();
        predict *= 5; // Predict location 5 (arbitrary choice) frames ahead
        Vector3 predictLoc = body.transform.position + predict;

        // Now we must find the normal to the path from the predicted location
        // We look at the normal for each line segment and pick out the closest one
        Vector3 normal = Vector3.zero;
        Vector3 target = Vector3.zero;
        float worldRecord = float.MaxValue;  // Start with a very high worldRecord distance that can easily be beaten

		int first;
		int last;
		
		if (curWP == null){
		    first = 0;
		    last = path.getNumberOfWayPoints();
		}
		else{
			first = path.getNextWayPointIndex(curWP.ID);
			last = first + 3;
		}
		
	    for (int i = first; i < last; ++i) {
			//Look at a line segment
	        WayPoint a = path.getWayPointAtIndex(i % path.getNumberOfWayPoints());
			WayPoint b = path.getWayPointAtIndex((i + 1) % path.getNumberOfWayPoints());
	
	        // Get the normal point to that line
	        Vector3 normalPoint = getNormalPoint(a, predictLoc);
	
	        // Check if normal is on line segment
	        Vector3 dir = a.getVectorToNextWayPoint();
	        // If it's not within the line segment, consider the normal to just be the end of the line segment (point b)
	        //if (da + db > line.mag()+1) {
	        if (normalPoint.x < Mathf.Min(a.getPosition().x, b.getPosition().x) || 
				normalPoint.x > Mathf.Max(a.getPosition().x, b.getPosition().x) || 
				normalPoint.z < Mathf.Min(a.getPosition().z, b.getPosition().z) || 
				normalPoint.z > Mathf.Max(a.getPosition().z, b.getPosition().z)) {
	            
				normalPoint = new Vector3(b.getPosition().x, b.getPosition().y, b.getPosition().z);
				
	            // If we're at the end we really want the next line segment for looking ahead
	            a = path.getWayPointAtIndex(path.getNextWayPointIndex(a.ID));
	            b = path.getWayPointAtIndex(path.getNextWayPointIndex(b.ID));
	            dir = a.getVectorToNextWayPoint();
	        }
	
	        // How far away are we from the path?
	        float d = Vector3.Distance(predictLoc, normalPoint);
	        // Did we beat the worldRecord and find the closest line segment?
	        if (d < worldRecord) {
	            worldRecord = d;
	            normal = normalPoint;
	            curWP = path.getWayPointAtIndex(i % path.getNumberOfWayPoints());
	
	            // Look at the direction of the line segment so we can seek a little bit ahead of the normal
	            dir.Normalize();
	            // arbitrary.  It seems to work fine though
	            dir *= 25;
	            target = new Vector3(normal.x, normal.y, normal.z);
	            target += dir;
	        }
	    }
		
		Vector3 ret = Vector3.zero;
        // Only if the distance is greater than the path's radius do we bother to steer
        if (worldRecord > attr.pathRadius) {
            ret = Seek(target);
        }
		else if (body.velocity.magnitude <= (attr.maxSpeed * 0.3)){
			ret = Seek (target); //If a guy slows way down, we need to kick them into gear
		}
	    return ret;
	}
	
	public Vector3 queue(Vector3 target, float [] ghostDistances, List<GameObject> ghosts){
		Vector3 ret = attr.seekWt * Seek (target);
		for (int i = 0; (i < ghostDistances.Length); ++i){
		    if (ghostDistances[i] != 0){   //Do nothing if the same guy
				
				if (ghostDistances[i] < attr.queueDistance){  //If a guy is close by
					Ghost temp = ghosts[i].GetComponent<Ghost>();
				    
					if (temp.Velocity.sqrMagnitude < body.velocity.sqrMagnitude){  //If the nearby guy is slow
						Vector3 vecToCenter = temp.transform.position - transform.position;
						
						if (Vector3.Dot (vecToCenter, transform.forward) > 0){ //Don't do anything if behind
				            ret += (attr.queueWeight * (1/ghostDistances[temp.getID()]) * -body.velocity);
						}
					}
				}
			}
		}
		return ret;
	}
	
	public Vector3 AvoidObstacle (GameObject obst, int safeDistance, int obRadius){ 
		dv = Vector3.zero;

		//vector from vehicle to center of obstacle
		Vector3 vecToCenter = obst.transform.position - transform.position;
		vecToCenter.y = 0; //Ignore going up

		float dist = vecToCenter.magnitude;

		// if too far to worry about, out of here
		if (dist > safeDistance + obRadius + attr.radius)
			return Vector3.zero;
		
		//if behind us, out of here
		if (Vector3.Dot (vecToCenter, transform.forward) < 0)
			return Vector3.zero;

		float rightDotVTC = Vector3.Dot (vecToCenter, transform.right);
		
		//if we can pass safely, out of here
		if (Mathf.Abs (rightDotVTC) > attr.radius + obRadius)
			return Vector3.zero;
				
		//obstacle on right so we steer to left
		if (rightDotVTC > 0)
			dv += transform.right * -attr.maxSpeed * safeDistance / dist;
		else
		//obstacle on left so we steer to right
			dv += transform.right * attr.maxSpeed * safeDistance / dist;
		
		return dv;	
	}
	
	private int estimateCostToGoal(WayPoint current, WayPoint goal){
		return attr.findCost * (Mathf.Abs(current.cemetaryRow - goal.cemetaryRow) + Mathf.Abs(current.cemetaryCol - goal.cemetaryCol));
	}
	
	private bool isWayPointInList(List<WayPoint> l, WayPoint w){
		bool found = false;
		for (int i = 0; (i < l.Count) && !found; ++i){
		    if (l[i].ID == w.ID){
			    found = true;	
			}
		}
		return found;
	}
	
	private Path constructPath(SortedDictionary<int, WayPoint> cameFrom, WayPoint goal){
		List<WayPoint> wayPoints = new List<WayPoint>();
		WayPoint current = cameFrom[goal.ID];
		
		int ID = 0;
		
		//WayPoints in the path live for more than just one frame, therefore they must be copied
		while (current.ID != -1){
			wayPoints.Insert (0, current.copy(ID++));
			current = cameFrom[current.ID];
		}
		wayPoints.Add (goal.copy (ID++));
		
		for (int i = 0; i < wayPoints.Count - 1; ++i){
		   wayPoints[i].setNextWaypoint(wayPoints [i + 1]);	
		}
		
		return new Path(wayPoints);
	}
	
	public Path findPath(Cemetary cem, WayPoint goal){
		
		GC.Collect(); //Run garbage collector
		GC.WaitForPendingFinalizers();
		
		//This method only works since unity is single threaded.  Otherwise, fscores from other ghosts will clash with each other
		
		WayPoint AStar = cem.getGate();
		
		//Create the open and close set
		List<WayPoint> openSet = new List<WayPoint>();
		
		List<WayPoint> closeSet = new List<WayPoint>();
		
	
		//First, get next path from gate
		openSet.Add (AStar.getNextWayPoint());
		closeSet.Add (AStar);
		
		//The wayPoint value came from the WayPoint ID key
		SortedDictionary<int, WayPoint> cameFrom = new SortedDictionary<int, WayPoint>();
		
		WayPoint current = AStar.getNextWayPoint();
		current.gScore = 0;
		current.fScore = current.gScore + estimateCostToGoal(current, goal);
		
		cameFrom.Add (current.ID, AStar);
		
		WayPoint [] neighbors = new WayPoint[4]; // possible to have 4 neighbors
		
		while ((openSet.Count != 0) && (current.ID != goal.ID)){
			openSet.Remove(current);
			closeSet.Add (current);
			
			neighbors[0] = current.getNorthWayPoint();
			neighbors[1] = current.getSouthWayPoint();
			neighbors[2] = current.getEastWayPoint();
			neighbors[3] = current.getWestWayPoint();
			
			
			foreach (WayPoint w in neighbors){
				if ((w != null) && (w.getType() == WayPoint.WPType.PATH)){
					int tentative_g_score = current.gScore + attr.findCost; //Always 10 away from neighbor
					int tentative_f_score = tentative_g_score + estimateCostToGoal(current, goal);
					
				    if (isWayPointInList(closeSet, w) && (tentative_f_score >= w.fScore)){ //If in closed list and f score is greater, continue
						//No op
					}
					
					else if (!isWayPointInList(openSet, w)){
						cameFrom[w.ID] = current;
					    w.fScore = tentative_f_score;
						w.gScore = tentative_g_score;
						openSet.Add (w);
					}
					
					else if (tentative_f_score < w.fScore){
						cameFrom[w.ID] = current;
					    w.fScore = tentative_f_score;	
						w.gScore = tentative_g_score;
					}
					
				}
			}
			
			int maxFScore = int.MaxValue;
			
			foreach (WayPoint w in openSet){
			    if(w.fScore < maxFScore){
				    maxFScore = w.fScore;
					current = w;
				}
			}
		}
		
		if (current.ID != goal.ID){
		    Debug.LogError("This should never happen, a fix is needed :(");	
		}
		
		//Clear memory
		openSet.Clear();
		closeSet.Clear ();
		
		GC.Collect(); //Run garbage collector
		GC.WaitForPendingFinalizers();
		
		return constructPath(cameFrom, goal);
	}
}
